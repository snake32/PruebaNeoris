package pe.com.pruebatecnica.config;

public class BadRequestException extends Exception {

    public BadRequestException(String message) {
        super(message);
    }

}