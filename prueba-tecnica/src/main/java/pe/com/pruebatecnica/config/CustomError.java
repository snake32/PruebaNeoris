package pe.com.pruebatecnica.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.SneakyThrows;

@Data
public class CustomError {

    private String mensaje;

    public CustomError(String mensaje) {
        super();
        this.mensaje = mensaje;
    }

    @Override
    @SneakyThrows
    public String toString() {
        return (new ObjectMapper()).writeValueAsString(this);
    }

}