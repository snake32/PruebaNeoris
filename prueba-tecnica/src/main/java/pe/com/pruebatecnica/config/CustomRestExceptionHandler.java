package pe.com.pruebatecnica.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({BadRequestException.class })
    public ResponseEntity<Object> handleFlashBadRequestException(BadRequestException ex){
        CustomError customError = new CustomError(ex.getMessage());
        return buildResponseEntity(customError);
    }

    private ResponseEntity<Object> buildResponseEntity(CustomError customError) {
        log.error("Error status={} message={} ",HttpStatus.BAD_REQUEST, customError.getMensaje());
        return new ResponseEntity<>(customError, HttpStatus.BAD_REQUEST);
    }

}