package pe.com.pruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.CustomerDTO;
import pe.com.pruebatecnica.service.CustomerService;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<CustomerDTO> findAll(){
        return customerService.findAll();
    }

    @PostMapping
    public CustomerDTO create(@RequestBody CustomerDTO customerDTO){
        return customerService.create(customerDTO);
    }

    @PutMapping
    public CustomerDTO update(@RequestBody CustomerDTO customerDTO){
        return customerService.update(customerDTO);
    }

    @DeleteMapping("/{idCustomer}")
    public void delete(@PathVariable Long idCustomer) throws BadRequestException {
       customerService.delete(idCustomer);
    }

}