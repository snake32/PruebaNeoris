package pe.com.pruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pe.com.pruebatecnica.dto.MovementDTO;
import pe.com.pruebatecnica.service.ReportService;

import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/account-statement")
    public List<MovementDTO> accountStatement(@RequestParam Long idPerson, @RequestParam String startDate, @RequestParam String endDate){
        return reportService.accountStatement(idPerson, startDate, endDate);
    }
}
