package pe.com.pruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.MovementDTO;
import pe.com.pruebatecnica.service.MovementsService;

import java.util.List;

@RestController
@RequestMapping("/movements")
public class MovementsController {

    @Autowired
    public MovementsService movementsService;

    @GetMapping
    public List<MovementDTO> findAll(){
        return movementsService.findAll();
    }

    @PostMapping
    public MovementDTO create(@RequestBody MovementDTO movementsDTO) throws BadRequestException {
        return movementsService.create(movementsDTO);
    }

    @PutMapping
    public MovementDTO update(@RequestBody MovementDTO movementsDTO){
        return movementsService.update(movementsDTO);
    }

    @DeleteMapping("/{idMovements}")
    public void delete(@PathVariable Long idMovements) throws BadRequestException {
        movementsService.delete(idMovements);
    }

}