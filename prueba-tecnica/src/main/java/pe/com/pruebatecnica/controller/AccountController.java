package pe.com.pruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.AccountDTO;
import pe.com.pruebatecnica.service.AccountService;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public List<AccountDTO> findAll(){
        return accountService.findAll();
    }

    @PostMapping
    public AccountDTO create(@RequestBody AccountDTO accountDTO){
        return accountService.create(accountDTO);
    }

    @PutMapping
    public AccountDTO update(@RequestBody AccountDTO accountDTO){
        return accountService.update(accountDTO);
    }

    @DeleteMapping("/{idAccount}")
    public void delete(@PathVariable Long idAccount) throws BadRequestException {
        accountService.delete(idAccount);
    }

}