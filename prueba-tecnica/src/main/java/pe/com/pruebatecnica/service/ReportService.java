package pe.com.pruebatecnica.service;

import pe.com.pruebatecnica.dto.MovementDTO;

import java.util.List;

public interface ReportService {

    List<MovementDTO>  accountStatement(Long idPerson, String startDate, String endDate);

}
