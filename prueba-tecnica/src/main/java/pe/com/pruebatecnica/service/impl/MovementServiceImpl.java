package pe.com.pruebatecnica.service.impl;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.MovementDTO;
import pe.com.pruebatecnica.entity.AccountEntity;
import pe.com.pruebatecnica.entity.MovementEntity;
import pe.com.pruebatecnica.mapper.MovementMapper;
import pe.com.pruebatecnica.repository.AccountRepository;
import pe.com.pruebatecnica.repository.MovementRepository;
import pe.com.pruebatecnica.service.MovementsService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MovementServiceImpl implements MovementsService {

    private MovementMapper mapper = Mappers.getMapper(MovementMapper.class);

    @Autowired
    private MovementRepository movementRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<MovementDTO> findAll() {
        List<MovementEntity> movementEntities = movementRepository.findAll();
        return movementEntities.stream().map(mapper::fromEntityToModel).collect(Collectors.toList());
    }

    @Override
    public MovementDTO create(MovementDTO movementDTO) throws BadRequestException {

        BigDecimal balanceFound;
        Long idAccount = movementDTO.getAccount().getIdAccount();

        MovementEntity movementEntity = mapper.fromModelToEntity(movementDTO);

        List<MovementEntity> movementEntities = movementRepository.findByAccount_IdAccount(
                movementDTO.getAccount().getIdAccount());

        if(movementEntities==null || movementEntities.isEmpty()){
            Optional<AccountEntity> accountEntity = accountRepository.findById(idAccount);
            balanceFound = accountEntity.get().getInitialBalance();
        } else {
            MovementEntity movementEntityFound = movementRepository.findByAccount(idAccount);
            balanceFound = movementEntityFound.getBalance();
        }

        if(movementEntity.getMovementType().equalsIgnoreCase("DEBITO")){
            movementEntity.setValue(movementEntity.getValue().multiply(new BigDecimal(-1)));
        }

        BigDecimal balance = balanceFound.add(movementEntity.getValue());

        if(balance.compareTo(BigDecimal.ZERO)<0) throw new BadRequestException("Saldo no disponible");

        movementEntity.setBalance(balance);
        movementEntity.setDate(new Date());
        movementEntity = movementRepository.save(movementEntity);

        return mapper.fromEntityToModel(movementEntity);
    }

    @Override
    public MovementDTO update(MovementDTO movementDTO){
        MovementEntity movementEntity = mapper.fromModelToEntity(movementDTO);
        movementEntity.setDate(new Date());
        movementEntity = movementRepository.save(movementEntity);
        return mapper.fromEntityToModel(movementEntity);
    }

    @Override
    public void delete(Long idMovements) throws BadRequestException {
        Optional<MovementEntity> movementEntityOpt = movementRepository.findById(idMovements);
        if(movementEntityOpt.isPresent()) throw new BadRequestException("Movimiento no encontrado");
        movementRepository.deleteById(idMovements);
    }

}