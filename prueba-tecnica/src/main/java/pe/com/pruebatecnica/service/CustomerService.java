package pe.com.pruebatecnica.service;

import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.CustomerDTO;

import java.util.List;

public interface CustomerService {

    List<CustomerDTO> findAll();
    CustomerDTO create(CustomerDTO customerDTO);
    CustomerDTO update(CustomerDTO customerDTO);
    void delete(Long idCustomer) throws BadRequestException;

}