package pe.com.pruebatecnica.service.impl;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.pruebatecnica.dto.MovementDTO;
import pe.com.pruebatecnica.entity.MovementEntity;
import pe.com.pruebatecnica.mapper.MovementMapper;
import pe.com.pruebatecnica.repository.MovementRepository;
import pe.com.pruebatecnica.service.ReportService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {

    private MovementMapper mapper = Mappers.getMapper(MovementMapper.class);

    @Autowired
    MovementRepository movementRepository;

    @Override
    public List<MovementDTO> accountStatement(Long idPerson, String startDate, String endDate) {
        List<MovementEntity> movementEntities = movementRepository.report(idPerson, startDate, endDate);
        return movementEntities.stream().map(mapper::fromEntityToModel).collect(Collectors.toList());
    }

}
