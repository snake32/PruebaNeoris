package pe.com.pruebatecnica.service.impl;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.CustomerDTO;
import pe.com.pruebatecnica.entity.CustomerEntity;
import pe.com.pruebatecnica.mapper.CustomerMapper;
import pe.com.pruebatecnica.repository.CustomerRepository;
import pe.com.pruebatecnica.service.CustomerService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerMapper mapper = Mappers.getMapper(CustomerMapper.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<CustomerDTO> findAll() {
        List<CustomerEntity> customerEntities = customerRepository.findAll();
        return customerEntities.stream().map(mapper::fromEntityToModel).collect(Collectors.toList());
    }

    @Override
    public CustomerDTO create(CustomerDTO customerDTO) {
        CustomerEntity customerEntity = mapper.fromModelToEntity(customerDTO);
        customerEntity = customerRepository.save(customerEntity);
        return mapper.fromEntityToModel(customerEntity);
    }

    @Override
    public CustomerDTO update(CustomerDTO customerDTO) {
        CustomerEntity customerEntity = mapper.fromModelToEntity(customerDTO);
        customerEntity = customerRepository.save(customerEntity);
        return mapper.fromEntityToModel(customerEntity);
    }

    @Override
    public void delete(Long idCustomer) throws BadRequestException {
        Optional<CustomerEntity> customerEntityOpt = customerRepository.findById(idCustomer);
        if(customerEntityOpt.isPresent()) throw new BadRequestException("Cliente no encontrado");
        customerRepository.deleteById(idCustomer);
    }

}