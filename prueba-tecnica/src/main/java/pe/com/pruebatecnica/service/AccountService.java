package pe.com.pruebatecnica.service;

import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.AccountDTO;

import java.util.List;

public interface AccountService {

    List<AccountDTO> findAll();
    AccountDTO create(AccountDTO accountDTO);
    AccountDTO update(AccountDTO accountDTO);
    void delete(Long idAccount) throws BadRequestException;

}