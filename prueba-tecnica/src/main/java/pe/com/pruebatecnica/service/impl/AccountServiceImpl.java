package pe.com.pruebatecnica.service.impl;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.AccountDTO;
import pe.com.pruebatecnica.entity.AccountEntity;
import pe.com.pruebatecnica.mapper.AccountMapper;
import pe.com.pruebatecnica.repository.AccountRepository;
import pe.com.pruebatecnica.service.AccountService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    private AccountMapper mapper = Mappers.getMapper(AccountMapper.class);

    @Autowired
    private AccountRepository accountRepository;

    public AccountServiceImpl() {
    }

    @Override
    public List<AccountDTO> findAll() {
        List<AccountEntity> accountEntities = accountRepository.findAll();
        return accountEntities.stream().map(mapper::fromEntityToModel).collect(Collectors.toList());
    }

    @Override
    public AccountDTO create(AccountDTO accountDTO) {
        AccountEntity accountEntity = mapper.fromModelToEntity(accountDTO);
        accountEntity = accountRepository.save(accountEntity);
        return mapper.fromEntityToModel(accountEntity);
    }

    @Override
    public AccountDTO update(AccountDTO accountDTO) {
        AccountEntity accountEntity = accountRepository.findById(accountDTO.getIdAccount()).get();
        accountEntity.setAccountType(accountDTO.getAccountType());
        accountEntity.setAccountNumber(accountDTO.getAccountNumber());
        accountEntity.setStatus(accountDTO.getStatus());
        accountEntity.setInitialBalance(accountDTO.getInitialBalance());
        accountEntity = accountRepository.save(accountEntity);
        return accountDTO;
    }

    @Override
    public void delete(Long idAccount) throws BadRequestException {
        Optional<AccountEntity> accountEntityOpt = accountRepository.findById(idAccount);
        if(accountEntityOpt.isPresent()) throw new BadRequestException("Cliente no encontrado");
        accountRepository.deleteById(idAccount);
    }

}