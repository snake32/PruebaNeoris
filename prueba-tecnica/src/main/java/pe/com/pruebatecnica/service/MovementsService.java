package pe.com.pruebatecnica.service;

import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.MovementDTO;

import java.util.List;

public interface MovementsService {

    List<MovementDTO> findAll();
    MovementDTO create(MovementDTO movementsDTO) throws BadRequestException;
    MovementDTO update(MovementDTO movementsDTO);
    void delete(Long idMovements) throws BadRequestException;

}
