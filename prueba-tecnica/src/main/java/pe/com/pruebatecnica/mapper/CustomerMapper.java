package pe.com.pruebatecnica.mapper;

import org.mapstruct.Mapper;
import pe.com.pruebatecnica.dto.CustomerDTO;
import pe.com.pruebatecnica.entity.CustomerEntity;

@Mapper
public interface CustomerMapper {

    CustomerDTO fromEntityToModel(CustomerEntity customerEntity);
    CustomerEntity fromModelToEntity(CustomerDTO customerDTO);

}