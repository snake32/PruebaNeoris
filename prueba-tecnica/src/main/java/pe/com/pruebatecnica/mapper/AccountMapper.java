package pe.com.pruebatecnica.mapper;

import org.mapstruct.Mapper;
import pe.com.pruebatecnica.dto.AccountDTO;
import pe.com.pruebatecnica.entity.AccountEntity;

@Mapper
public interface AccountMapper {

    AccountDTO fromEntityToModel(AccountEntity accountEntity);
    AccountEntity fromModelToEntity(AccountDTO accountDTO);

}