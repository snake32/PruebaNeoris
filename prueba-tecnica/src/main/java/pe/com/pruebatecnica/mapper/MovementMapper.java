package pe.com.pruebatecnica.mapper;

import org.mapstruct.Mapper;
import pe.com.pruebatecnica.dto.MovementDTO;
import pe.com.pruebatecnica.entity.MovementEntity;

@Mapper
public interface MovementMapper {

    MovementDTO fromEntityToModel(MovementEntity movementsEntity);
    MovementEntity fromModelToEntity(MovementDTO movementsDTO);

}