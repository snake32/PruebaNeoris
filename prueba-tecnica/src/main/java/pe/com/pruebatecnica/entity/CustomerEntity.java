package pe.com.pruebatecnica.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name= "customer")
public class CustomerEntity extends PersonEntity {

    @Column(name = "password", length = 60)
    private String password;

    @Column(name = "status")
    private Boolean status;

}