package pe.com.pruebatecnica.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name= "movement")
public class MovementEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_movement")
    private Long idMovement;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_account")
    private AccountEntity account;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "movement_type")
    private String movementType;

    @Column(name = "value", precision = 7, scale = 2)
    private BigDecimal value;

    @Column(name = "balance", precision = 7, scale = 2)
    private BigDecimal balance;

}