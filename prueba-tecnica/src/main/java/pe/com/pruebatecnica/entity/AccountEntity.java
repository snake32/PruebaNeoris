package pe.com.pruebatecnica.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name= "account")
public class AccountEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_account")
    private Long idAccount;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_customer")
    private CustomerEntity customer;

    @Column(name = "account_type")
    private String accountType;

    @Column(name = "account_number")
    private Integer accountNumber;

    @Column(name = "initial_balance", precision = 7, scale = 2)
    private BigDecimal initialBalance;

    @Column(name = "status")
    private Boolean status;

}