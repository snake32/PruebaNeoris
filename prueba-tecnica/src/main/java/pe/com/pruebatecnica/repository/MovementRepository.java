package pe.com.pruebatecnica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.pruebatecnica.entity.MovementEntity;

import java.util.List;

@Repository
public interface MovementRepository extends JpaRepository<MovementEntity, Long> {

    List<MovementEntity> findByAccount_IdAccount(Long idAccount);

    @Query(value = "SELECT * FROM movement WHERE id_account = :idAccount order by id_movement desc limit 1",nativeQuery = true)
    MovementEntity findByAccount(@Param("idAccount") Long idAccount);


    @Query(value = "SELECT m.* FROM movement m INNER JOIN account a ON m.id_account = a.id_account " +
            "WHERE a.id_customer = :idPerson AND m.date BETWEEN :startDate AND :endDate order by m.id_movement",nativeQuery = true)
    List<MovementEntity> report(@Param("idPerson") Long idPerson, @Param("startDate") String startDate, @Param("endDate") String endDate);

}