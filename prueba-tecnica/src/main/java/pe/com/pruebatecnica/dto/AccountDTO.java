package pe.com.pruebatecnica.dto;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class AccountDTO {

    private Long idAccount;
    private CustomerDTO customer;
    private String accountType;
    private Integer accountNumber;
    private BigDecimal initialBalance;
    private Boolean status;

}