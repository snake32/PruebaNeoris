package pe.com.pruebatecnica.dto;

import lombok.Data;

@Data
public class PersonDTO {

    private Long idPerson;
    private String name;
    private String gender;
    private Integer age;
    private String direction;
    private String phone;

}
