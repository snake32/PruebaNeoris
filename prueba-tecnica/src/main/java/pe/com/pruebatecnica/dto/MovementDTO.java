package pe.com.pruebatecnica.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MovementDTO {

    private Long idMovement;
    private AccountDTO account;

    @JsonFormat(pattern="dd-MM-yyyy")
    private Date date;

    private String movementType;
    private BigDecimal value;
    private BigDecimal balance;

}