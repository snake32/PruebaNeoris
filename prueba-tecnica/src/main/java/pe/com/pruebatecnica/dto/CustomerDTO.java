package pe.com.pruebatecnica.dto;

import lombok.Data;

@Data
public class CustomerDTO extends PersonDTO{

    private String password;
    private Boolean status;

}