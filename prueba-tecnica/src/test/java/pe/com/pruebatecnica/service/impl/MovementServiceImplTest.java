package pe.com.pruebatecnica.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.AccountDTO;
import pe.com.pruebatecnica.dto.MovementDTO;
import pe.com.pruebatecnica.entity.AccountEntity;
import pe.com.pruebatecnica.entity.MovementEntity;
import pe.com.pruebatecnica.repository.AccountRepository;
import pe.com.pruebatecnica.repository.MovementRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class MovementServiceImplTest {

    @Mock
    private MovementRepository movementRepository;

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private MovementServiceImpl movementServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll() {
        when(movementRepository.findAll()).thenReturn(getMovements());
        assertNotNull(movementServiceImpl.findAll());
    }

    @Test
    void create() throws BadRequestException {
        when(accountRepository.findById(any())).thenReturn(Optional.of(getAccountEntity()));
        when(movementRepository.save(any())).thenReturn(new MovementEntity());
        assertNotNull(movementServiceImpl.create(getMovementDTO()));
    }

    @Test
    void update() throws BadRequestException {
        when(movementRepository.findById(any())).thenReturn(Optional.empty());
        when(movementRepository.save(any())).thenReturn(new MovementEntity());
        assertNotNull(movementServiceImpl.update(getMovementDTO()));
    }

    @Test
    void delete() throws BadRequestException {
        movementServiceImpl.delete(anyLong());
    }

    List<MovementEntity> getMovements(){
        return new ArrayList<>();
    }

    MovementDTO getMovementDTO(){
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setIdAccount(new Long(1));

        MovementDTO movementDTO = new MovementDTO();
        movementDTO.setIdMovement(new Long(1));
        movementDTO.setAccount(accountDTO);
        movementDTO.setMovementType("CREDITO");
        movementDTO.setValue(new BigDecimal(10));

        return movementDTO;
    }

    AccountEntity getAccountEntity(){
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setInitialBalance(new BigDecimal(100));
        return accountEntity;
    }

}