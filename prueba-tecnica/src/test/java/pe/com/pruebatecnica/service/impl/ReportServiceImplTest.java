package pe.com.pruebatecnica.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pe.com.pruebatecnica.entity.MovementEntity;
import pe.com.pruebatecnica.repository.MovementRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class ReportServiceImplTest {

    @Mock
    private MovementRepository movementRepository;

    @InjectMocks
    private ReportServiceImpl reportServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void accountStatement() {
        when(movementRepository.report(anyLong(), anyString(), anyString())).thenReturn(getMovements());
        assertNotNull(reportServiceImpl.accountStatement(anyLong(), anyString(), anyString()));
    }

    List<MovementEntity> getMovements(){
        return new ArrayList<>();
    }

}