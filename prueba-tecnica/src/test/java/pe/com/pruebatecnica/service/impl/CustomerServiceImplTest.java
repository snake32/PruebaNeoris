package pe.com.pruebatecnica.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.CustomerDTO;
import pe.com.pruebatecnica.entity.CustomerEntity;
import pe.com.pruebatecnica.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class CustomerServiceImplTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll() {
        when(customerRepository.findAll()).thenReturn(getCustomers());
        assertNotNull(customerServiceImpl.findAll());
    }

    @Test
    void create() {
        when(customerRepository.save(any())).thenReturn(new CustomerEntity());
        assertNotNull(customerServiceImpl.create(any()));
    }

    @Test
    void update() throws BadRequestException {
        when(customerRepository.findById(any())).thenReturn(Optional.empty());
        when(customerRepository.save(any())).thenReturn(new CustomerEntity());
        assertNotNull(customerServiceImpl.update(getCustomerDTO()));
    }

    @Test
    void delete() throws BadRequestException {
         customerServiceImpl.delete(anyLong());
    }

    List<CustomerEntity> getCustomers(){
        return new ArrayList<>();
    }

    CustomerDTO getCustomerDTO(){
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setIdPerson(new Long(1));
        return customerDTO;
    }

}