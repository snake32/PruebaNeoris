package pe.com.pruebatecnica.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pe.com.pruebatecnica.config.BadRequestException;
import pe.com.pruebatecnica.dto.AccountDTO;
import pe.com.pruebatecnica.entity.AccountEntity;
import pe.com.pruebatecnica.repository.AccountRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountServiceImpl accountServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll() {
        when(accountRepository.findAll()).thenReturn(getAccounts());
        assertNotNull(accountServiceImpl.findAll());
    }

    @Test
    void create() {
        when(accountRepository.save(any())).thenReturn(new AccountEntity());
        assertNotNull(accountServiceImpl.create(any()));
    }

    @Test
    void update() throws BadRequestException {
        when(accountRepository.findById(any())).thenReturn(Optional.empty());
        when(accountRepository.save(any())).thenReturn(new AccountEntity());
        assertNotNull(accountServiceImpl.update(getAccountDTO()));
    }

    @Test
    void delete() throws BadRequestException {
        accountServiceImpl.delete(anyLong());
    }

    List<AccountEntity> getAccounts(){
        return new ArrayList<>();
    }

    AccountDTO getAccountDTO(){
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setIdAccount(new Long(1));
        return accountDTO;
    }

}