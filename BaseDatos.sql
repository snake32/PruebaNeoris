use prueba_tecnica;
create table account(
	id_account int not null primary key auto_increment,
    id_customer int,
    account_type varchar(50),
    account_number int,
    initial_balance decimal(7,2),
    status boolean
);
insert into account(id_customer,account_type,account_number,initial_balance,status) values(1,'Ahorro',478758,2000,true);
insert into account(id_customer,account_type,account_number,initial_balance,status) values(2,'Corriente',225487,100,true);
insert into account(id_customer,account_type,account_number,initial_balance,status) values(3,'Ahorro',495878,0,true);
insert into account(id_customer,account_type,account_number,initial_balance,status) values(2,'Corriente',496825,2000,true);
insert into account(id_customer,account_type,account_number,initial_balance,status) values(1,'Corriente',585545,1000,true);

create table movement(
	id_movement int not null primary key auto_increment,
    id_account int,
    date datetime,
    movement_type varchar(60),
    value decimal(7,2),
    balance decimal(7,2)
);
insert into movement(id_account,date,movement_type,value,balance) values(1,now(),'CREDITO','510','2510');
insert into movement(id_account,date,movement_type,value,balance) values(1,now(),'DEBITO','-510','2000');

create table person(
	id_person int not null primary key auto_increment,
    dtype varchar(31)not null,
    name varchar(200),
    gender varchar(60),
    age int,
    direction varchar(200),
    phone varchar(60),
    password varchar(60),
    status boolean
);
insert into person(dtype,name,gender,age,direction,phone,password,status) values('CustomerEntity','Jose Lema','masculino',23,'Otavalo sn y principal','098254785','1234',true);
insert into person(dtype,name,gender,age,direction,phone,password,status) values('CustomerEntity','Marianela Montalvo','femenino',20,'Amazonas y NNUU','097548965','5678',true);
insert into person(dtype,name,gender,age,direction,phone,password,status) values('CustomerEntity','Juan Osorio','masculino',31,'13 junio y Equinoccial','098874587','1245',true);